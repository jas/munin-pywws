#raw#
#timezone utc#
MUNIN-PYWWS created #idx '%Y-%m-%dT%H:%M:%SZ'#

graph_title Outdoor Temperature

graph_vlabel Temperature in C

graph_category Weather

temp_out_c.label Outdoor Temperature

graph_args --base 1000 -l 0

temp_out_c.value #temp_out '%0.1f'#

#!eof
